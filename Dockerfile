FROM httpd
LABEL username="Minh" email="minh.ngo@dawsoncollege.qc.ca"
WORKDIR /usr/local/apache2/htdocs/

COPY app/ /usr/local/apache2/htdocs/
